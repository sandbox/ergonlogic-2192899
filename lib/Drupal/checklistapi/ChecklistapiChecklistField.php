<?php

/**
 * @file
 * Class for Checklist API checklists as fields.
 */

/**
 * Defines the checklist class.
 */
class ChecklistapiChecklistField extends ChecklistapiChecklist {

  /**
   * The checklist field data.
   *
   * @var array
   */
  public $field_data;

  /**
   * The checklist Delta.
   *
   * @var string
   */
  public $delta;

  /**
   * Constructs a ChecklistapiChecklistField object.
   *
   * @param array $definition
   *   A checklist definition, as returned by checklistapi_get_checklist_info().
   * @param string $field_data (optional)
   *   The field data for the entity to which the checklist is attached.
   * @param string $delta (optional)
   *   The checklist delta, allowing multiple checklist fields to be attached to the same entity.
   */
  public function __construct(array $definition, $field_data = array(), $delta = 0) {
    $this->delta = $delta;
    $this->field_data = $field_data;
    parent::__construct($definition);

    // Override checklist path to allow clearing checklist fields.
    // See: checklistapi_checklist_form().
    $entity_info = entity_get_info($field_data['entity_type']);
    $id_key = $entity_info['entity keys']['id'];
    $this->path = $field_data['entity_type'] . '/' . $field_data['entity']->$id_key . '/checklist/'. $definition['#id'] . '/' . $delta;
    $this->redirect = $field_data['entity_type'] . '/' . $field_data['entity']->$id_key;
  }

  /**
   * Extract saved progress from field.
   */
  public function getSavedProgress() {
    return unserialize($this->field_data['entity']->{$this->field_data['field']['field_name']}[LANGUAGE_NONE][$this->delta]['progress']);
  }

  /**
   * Clears the saved progress for the checklist.
   *
   * @param string $entity_name
   *   The name of the entity on which we're clearing the checklist field progress.
   */
  public function clearSavedProgress($entity_name = 'this entity') {
    $this->setSavedProgress(array());
    drupal_set_message(t('%title saved progress on %name has been cleared.', array(
      '%title' => $this->title,
      '%name' => $entity_name,
    )));
  }

  /**
   * Determines whether the checklist has saved progress.
   *
   * @return bool
   *   TRUE if the checklist has saved progress, or FALSE if it doesn't.
   */
  public function hasSavedProgress() {
    return (bool) $this->getSavedProgress();
  }

  /**
   * Save our progress to the field storage.
   */
  public function setSavedProgress($progress) {
    $data = $this->field_data;
    $data['entity']->{$data['field']['field_name']}[LANGUAGE_NONE][$this->delta]['progress'] = serialize($progress);
    field_attach_update($data['entity_type'], $data['entity']);
  }

  /**
   * Determines whether the current user has access to the checklist.
   * @todo: FieldAPI
   * @param string $operation
   *   The operation to test access for. Possible values are "view", "edit", and
   *   "any". Defaults to "any".
   *
   * @return bool
   *   Returns TRUE if the user has access, or FALSE if not.
   */
  public function userHasAccess($operation = 'any') {
    return checklistapi_checklist_field_access($this->id, $operation, $this->field_data['entity_type']);
  }

}
