<?php
/**
 * @file
 * Field API hooks for the Checklist API.
 */

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function checklistapi_field_info() {
  return array(
    'checklist' => array(
      'label' => t('Checklist'),
      'description' => t('Provide a checklist field.'),
      'default_widget' => 'checklist_select',
      'default_formatter' => 'checklist_default',
      'columns' => array('checklist', 'progress'),
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function checklistapi_field_is_empty($item, $field) {
  return empty($item['checklist']);
}

/**
 * Implements hook_field_formatter_info().
 *
 * @see checklistapi_field_formatter_view()
 */
function checklistapi_field_formatter_info() {
  return array(
    // This formatter just displays the checklistapi form.
    'checklist_default' => array(
      'label' => t('Default checklistapi formatter'),
      'field types' => array('checklist'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 * The formatter simply displays the checklist form as markup.
 *
 * @see checklistapi_field_formatter_info()
 */
function checklistapi_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  include_once(drupal_get_path('module', 'checklistapi') . '/checklistapi.pages.inc');
  // Allow data from the field to be passed into the checklist form, object, etc.
  $field_data = array(
    'entity_type' => $entity_type,
    'entity' => $entity,
    'field' => $field,
  );
  $element = array();
  switch ($display['type']) {
    // This formatter simply outputs the default checklistapi form.
    case 'checklist_default':
      foreach ($items as $delta => $item) {
        if (checklistapi_checklist_field_access($item['checklist'], $operation = 'view', $entity_type)) {
          $checklist = drupal_get_form('checklistapi_checklist_form', $item['checklist'], $field_data, $delta);
          $element[$delta] = array(
            '#markup' => drupal_render($checklist),
          );
        }
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 *
 * A simple select widget is provided.
 *
 * @see checklistapi_field_widget_form()
 */
function checklistapi_field_widget_info() {
  return array(
    'checklist_select' => array(
      'label' => t('Select which checklist to attach'),
      'field types' => array('checklist'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 *
 * hook_widget_form() is where Drupal tells us to create form elements for
 * our field's widget.
 *
 * We provide a simple list of the available checklists defined in code.
 */
function checklistapi_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Checklist selection widget.
  $value = isset($items[$delta]['checklist']) ? $items[$delta]['checklist'] : '';
  $widget = $element;
  $widget['#delta'] = $delta;

  $checklists = checklistapi_get_checklist_info();
  $options = array('- none -');
  foreach ($checklists as $id => $checklist) {
    $options[$id] = $checklist['#title'];
  }

  if (checklistapi_checklist_field_access($id, 'administer', $instance['entity_type'])) {
    switch ($instance['widget']['type']) {
      case 'checklist_select':
        $widget += array(
          '#type' => 'select',
          '#default_value' => $value,
          '#options' => $options,
        );
        break;
    }
  }

  // Persist progress across entity saves.
  $progress = $element;
  $progress['#delta'] = $delta;
  $progress['#type'] = 'hidden';
  $progress['#value'] = isset($items[$delta]['progress']) ? $items[$delta]['progress'] : '';

  $element['checklist'] = $widget;
  $element['progress'] = $progress;
  return $element;
}

/**
 * Page callback: Form constructor for "Clear saved progress" confirmation form
 * for checklist fields.
 *
 * @param string $entity_type
 *   The type of entity to which the checklist field is attached.
 * @param string $entity_id
 *   The entity ID.
 * @param string $id
 *   The checklist ID.
 * @param string $field
 *   The checklist field (as returned by field_info_field_map()).
 * @param string $delta
 *   The field instance.
 *
 * @see checklistapi_menu()
 *
 * @ingroup forms
 */
function checklistapi_checklist_field_clear_confirm($form, &$form_state, $entity_type, $entity_id, $id, $field, $delta) {
  $entity_info = entity_get_info($entity_type);
  $entity_label = isset($entity_info['entity keys']['label']) ? $entity_info['entity keys']['label'] : 'name';
  $entity = entity_load($entity_type, array($entity_id));
  $entity = $entity[$entity_id];
  $field_data = array(
    'entity_type' => $entity_type,
    'entity' => $entity,
    'field' => $field,
  );

  $checklist = checklistapi_checklist_load($id, $field_data, $delta);
  $form['#checklist'] = $checklist;
  $form['#checklist']->entity_name = $entity_name = $entity->$entity_label;
  $question = t('Are you sure you want to clear %title saved progress on %name?', array(
    '%title' => $checklist->title,
    '%name' => $entity_name,
  ));
  $description = t('All progress details will be erased. This action cannot be undone.');
  $yes = t('Clear');
  $no = t('Cancel');
  return confirm_form($form, $question, $checklist->redirect, $description, $yes, $no);
}

/**
 * Form submission handler for checklistapi_checklist_field_clear_confirm().
 */
function checklistapi_checklist_field_clear_confirm_submit($form, &$form_state) {
  // If user confirmed, clear saved progress.
  if ($form_state['values']['confirm']) {
    $form['#checklist']->clearSavedProgress($form['#checklist']->entity_name);
  }

  // Redirect back to entity.
  $form_state['redirect'] = $form['#checklist']->redirect;
}

/**
 * Access callback: Checks the current user's access to a given checklist.
 *
 * @param string $id
 *   The checklist ID.
 * @param string $operation
 *   (optional) The operation to test access for. Accepted values are "view",
 *   "edit", "administer", and "any". Defaults to "any".
 * @param string $entity_type
 *   The entity type on which to grant access.
 *
 * @return bool
 *   Returns TRUE if the current user has access to perform a given operation on
 *   the specified checklist on the specified entity type, or FALSE if not.
 */
function checklistapi_checklist_field_access($id, $operation = 'any', $entity_type) {
  // Allow superusers to administer content types.
  if (user_access('bypass node access') || user_access('administer content types')) {
    return TRUE;
  }

  $all_operations = array('view', 'edit', 'administer', 'any');
  if (!in_array($operation, $all_operations)) {
    throw new Exception(t('No such operation "@operation"', array(
      '@operation' => $operation,
    )));
  }

  $access['administer'] = user_access('administer checklistapi checklist fields on ' . $entity_type . ' entities');
  $access['edit'] = $access['administer']
                 || user_access('edit any checklistapi checklist')
                 || user_access('edit any checklistapi checklist field on ' . $entity_type)
                 || user_access('edit ' . $id . ' checklistapi checklist field on ' . $entity_type);
  $access['view'] = $access['edit']
                 || user_access('view any checklistapi checklist')
                 || user_access('view any checklistapi checklist field on ' . $entity_type)
                 || user_access('view ' . $id . ' checklistapi checklist field on ' . $entity_type);
  $access['any'] = $access['view'] || $access['edit'] || $access['administer'];
  return $access[$operation];
}
